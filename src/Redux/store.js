import { configureStore } from '@reduxjs/toolkit';
import AuthReducer from './reducers/Auth';
import TodosReducer from './reducers/Todo';

export default configureStore({
	reducer: {
		auth: AuthReducer,
		todo: TodosReducer,
	},
});
