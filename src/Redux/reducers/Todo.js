import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { nanoid } from 'nanoid';
import axios from 'axios';
export const addTask = createAsyncThunk('tasks/add', async (payload) => {
	const resp = await axios
		.post(`${process.env.REACT_APP_API_URL}/task`, payload, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json',
			},
		})
		.then((data) => {
			// console.log('dataaa', data);
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
export const getAllTask = createAsyncThunk('tasks/getall', async () => {
	const resp = await axios
		.get(`${process.env.REACT_APP_API_URL}/task`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json',
			},
		})
		.then((data) => {
			console.log('dataaa', data);
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
export const getTask = createAsyncThunk('tasks/getbyid', async (id) => {
	const resp = await axios
		.get(`${process.env.REACT_APP_API_URL}/task/${id}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json',
			},
		})
		.then((data) => {
			console.log('dataaa', data);
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
export const editTask = createAsyncThunk('tasks/edit', async (payload) => {
	const resp = await axios
		.put(
			`${process.env.REACT_APP_API_URL}/task/${payload.id}`,
			{ completed: payload.completed },
			{
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
					'Content-Type': 'application/json',
				},
			}
		)
		.then((data) => {
			console.log('dataaa', data);
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
export const deleteTask = createAsyncThunk('tasks/delete', async (payload) => {
	const resp = await axios
		.delete(
			`${process.env.REACT_APP_API_URL}/task/${payload.id}`,

			{
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
		.then((data) => {
			console.log('dataaa', data);
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
export const getCompletedOrPaginationTask = createAsyncThunk(
	'tasks/delete',
	async (payload) => {
		const resp = await axios
			.get(
				`${process.env.REACT_APP_API_URL}/task`,

				{
					params: payload,
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`,
					},
				}
			)
			.then((data) => {
				console.log('dataaa', data);
				return data.data;
			})
			.catch((err) => {
				console.log('error', err);
				return err.message;
			});
		return resp;
	}
);

const initialState = {
	todos: [],
	todo: null,
};

export const todoSlice = createSlice({
	name: 'todos',
	initialState: initialState,
	reducers: {
		// addTask: (state, action) => {
		// 	var date = new Date();
		// 	var current_date =
		// 		date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
		// 	const todo = {
		// 		id: nanoid(),
		// 		description: action.payload.description,
		// 		completed: action.payload.completed,
		// 		date: current_date,
		// 	};
		// 	state.todos.push(todo);
		// },
		// toggleComplete: (state, action) => {
		// 	const index = state.findIndex((todo) => todo.id === action.payload.id);
		// 	state[index].completed = action.payload.completed;
		// },
		deleteTask: (state, action) => {
			return state.todos.filter((todo) => todo.id !== action.payload.data.id);
		},
	},
	extraReducers: {
		[getAllTask.fulfilled]: (state, action) => {
			console.log('action', action.payload.data);
			state.todos = action.payload.data;
		},
		[addTask.fulfilled]: (state, action) => {
			console.log('todoooo', action.payload);
			state.todos.push(action.payload.data);
		},
		[getTask.fulfilled]: (state, action) => {
			console.log('todoooo', action.payload);
			state.todo = action.payload.data;
		},
		[editTask.fulfilled]: (state, action) => {
			console.log('todoooo', action.payload);
			const index = state.todos.findIndex(
				(todo) => todo.id === action.payload.data.id
			);
			state.todos[index].completed = action.payload.data.completed;
		},
		[getCompletedOrPaginationTask.fulfilled]: (state, action) => {
			console.log('todoooo', action.payload);
			state.todos = action.payload.data;
		},
	},
});

export const { addTodo, toggleComplete, deleteTodo } = todoSlice.actions;

export default todoSlice.reducer;
