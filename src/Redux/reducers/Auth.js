import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { nanoid } from 'nanoid';

export const signup = createAsyncThunk('user/signup', async (payload) => {
	const resp = await axios
		.post(`${process.env.REACT_APP_API_URL}/user/register`, payload, {
			headers: {
				'Content-Type': 'application/json',
			},
		})
		.then((data) => {
			console.log('dataaa', data);
			localStorage.setItem('token', data.data.token);
			window.location.replace('/home');
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			alert(err.message);
			return err.message;
		});
	return resp;
});
export const signin = createAsyncThunk('user/signin', async (payload) => {
	const resp = await axios
		.post(`${process.env.REACT_APP_API_URL}/user/login`, payload, {
			headers: {
				'Content-Type': 'application/json',
			},
		})
		.then((data) => {
			console.log('dataaa', data);
			window.location.replace('/home');
			localStorage.setItem('token', data.data.token);
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
export const logout = createAsyncThunk('user/logout', async () => {
	const resp = await axios
		.post(
			`${process.env.REACT_APP_API_URL}/user/logout`,
			{},
			{
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
		.then((data) => {
			localStorage.removeItem('token');
			window.location.replace('/');
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
export const getuserProfile = createAsyncThunk('user/profile', async () => {
	const resp = await axios
		.get(
			`${process.env.REACT_APP_API_URL}/user/me`,

			{
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			}
		)
		.then((data) => {
			console.log('dataaa', data);
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});

export const updateUserProfile = createAsyncThunk(
	'user/update',
	async (payload) => {
		const resp = await axios
			.put(`${process.env.REACT_APP_API_URL}/user/me`, payload, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			})
			.then((data) => {
				console.log('dataaa', data);
				window.location.reload();
				return data.data;
			})
			.catch((err) => {
				console.log('error', err);
				alert(err.message);
				return err.message;
			});
		return resp;
	}
);
export const uploaUserdImg = createAsyncThunk(
	'user/avatar',
	async (payload) => {
		const resp = await axios
			.post(`${process.env.REACT_APP_API_URL}/user/me/avatar`, payload, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			})
			.then((data) => {
				console.log('dataaa', data);
				window.location.reload();
				return data.data;
			})
			.catch((err) => {
				console.log('error', err);
				alert(err.message);
				return err.message;
			});
		return resp;
	}
);
export const deleteUserImage = createAsyncThunk(
	'user/deletAvatar',
	async () => {
		const resp = await axios
			.delete(`${process.env.REACT_APP_API_URL}/user/me/avatar`, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`,
				},
			})
			.then((data) => {
				console.log('dataaa', data);
				window.location.reload();
				return data.data;
			})
			.catch((err) => {
				console.log('error', err);
				alert(err.message);
				return err.message;
			});
		return resp;
	}
);
export const deleteUserProfile = createAsyncThunk('user/update', async () => {
	const resp = await axios
		.delete(`${process.env.REACT_APP_API_URL}/user/me`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
			},
		})
		.then((data) => {
			console.log('dataaa', data);
			localStorage.removeItem('token');
			window.location.replace('/signup');
			return data.data;
		})
		.catch((err) => {
			console.log('error', err);
			return err.message;
		});
	return resp;
});
const initialState = {
	token: localStorage.getItem('token') ? localStorage.getItem('token') : '',
	id: null,
	name: '',
	email: '',
	age: '',
};

export const AuthSlice = createSlice({
	name: 'todos',
	initialState: initialState,
	reducers: {
		logout: (state, action) => {
			state.token = '';
		},
		deleteUserProfile: (state, action) => {
			state.token = '';
			state.name = '';
			state.email = '';
			state.id = '';
			state.age = null;
		},
		uploaUserdImg: (state, action) => {
			return state;
		},
		deleteUserImage: (state, action) => {
			return state;
		},
	},
	extraReducers: {
		[signup.fulfilled]: (state, action) => {
			console.log('action', action);
			state.token = action.payload.token;

			state.id = action.payload.user._id;
			state.name = action.payload.user.name;
			state.email = action.payload.user.email;
			state.age = action.payload.user.age;
		},
		[signin.fulfilled]: (state, action) => {
			console.log('action', action);
			state.token = action.payload.token;

			state.id = action.payload.user._id;
			state.name = action.payload.user.name;
			state.email = action.payload.user.email;
			state.age = action.payload.user.age;
		},
		[getuserProfile.fulfilled]: (state, action) => {
			state.id = action.payload._id;
			state.name = action.payload.name;
			state.email = action.payload.email;
			state.age = action.payload.age;
		},
		[updateUserProfile.fulfilled]: (state, action) => {
			state.id = action.payload._id;
			state.name = action.payload.name;
			state.email = action.payload.email;
			state.age = action.payload.age;
		},
	},
});

// export const { uploaUserdImg } = AuthSlice.actions;

export default AuthSlice.reducer;
