import React from 'react';
import img from '../../images/Rectangle 4.png';
import { useSelector, useDispatch } from 'react-redux';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { FilledInput } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { FormLabel } from '@mui/material';
import { signup } from '../../Redux/reducers/Auth';
import { useNavigate } from 'react-router-dom';

function Signup() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			email: data.get('email'),
			password: data.get('password'),
			name: data.get('name'),
			age: data.get('age'),
		});
		dispatch(
			signup({
				email: data.get('email'),
				password: data.get('password'),
				name: data.get('name'),
				age: data.get('age'),
			})
		);
	};

	return (
		<div style={{ textAlign: 'center', marginTop: '50px ' }}>
			<img src={img} alt='' />
			<Container component='main' maxWidth='xs' dir='ltr'>
				<CssBaseline />
				<Box
					sx={{
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
					}}>
					<Box
						component='form'
						onSubmit={handleSubmit}
						noValidate
						sx={{ mt: 1 }}>
						<FilledInput
							style={{ backgroundColor: 'white', marginBottom: '5px' }}
							margin='normal'
							fullWidth
							required
							name='name'
							type='text'
							id='name'
							placeholder='fill user name'
							autoComplete='email'></FilledInput>
						<FilledInput
							style={{ backgroundColor: 'white', marginBottom: '5px' }}
							margin='normal'
							fullWidth
							required
							name='email'
							type='email'
							id='email'
							placeholder='fill user email'
							autoComplete='email'></FilledInput>
						<FilledInput
							style={{
								backgroundColor: 'white',
								margin: '5px 0px',
							}}
							margin='normal'
							fullWidth
							required
							name='password'
							type='password'
							id='password'
							placeholder='fill uesr passowrd'></FilledInput>
						<FilledInput
							style={{
								backgroundColor: 'white',
								margin: '5px 0px',
							}}
							required
							margin='normal'
							fullWidth
							name='age'
							type='number'
							id='age'
							placeholder='fill uesr age'></FilledInput>

						<Button
							style={{
								borderRadius: '30px',
							}}
							type='submit'
							fullWidth
							variant='contained'
							sx={{ mt: 3, mb: 2 }}>
							{' '}
							Signup
						</Button>

						<Button
							style={{
								backgroundColor: '#FFE2A7 ',
								color: ' #3F2525',
								borderRadius: '30px',
							}}
							onClick={() => navigate('/signin')}
							fullWidth
							variant='contained'
							sx={{ mt: 3, mb: 2 }}>
							Login
						</Button>
					</Box>
				</Box>
			</Container>
		</div>
	);
}

export default Signup;
