import React from 'react';
import img from '../../images/Rectangle 4.png';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { FilledInput, InputLabel } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { FormLabel } from '@mui/material';
import { useDispatch } from 'react-redux';
import { signin } from '../../Redux/reducers/Auth';
import { Navigate, useNavigate } from 'react-router-dom';
function Signin() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			email: data.get('email'),
			password: data.get('password'),
		});
		dispatch(
			signin({ email: data.get('email'), password: data.get('password') })
		);
	};
	return (
		<div style={{ textAlign: 'center', marginTop: '50px ' }}>
			<img src={img} alt='' />
			<Container component='main' maxWidth='xs' dir='ltr'>
				<Box
					sx={{
						marginTop: 8,
						display: 'flex',
						flexDirection: 'column',
						alignItems: 'center',
					}}>
					<Box
						component='form'
						onSubmit={handleSubmit}
						noValidate
						sx={{ mt: 1 }}>
						<FilledInput
							aria-label='ggggggggg'
							style={{ backgroundColor: 'white', marginBottom: '15px' }}
							margin='normal'
							fullWidth
							required
							name='email'
							type='email'
							id='email'
							placeholder='fill email'
							autoComplete='email'></FilledInput>

						<FilledInput
							style={{
								backgroundColor: 'white',
								margin: '15px 0px',
							}}
							required
							margin='normal'
							fullWidth
							name='password'
							type='password'
							id='password'
							placeholder='fill uesr passowrd'
							autoComplete='current-password'></FilledInput>
						<Button
							style={{
								borderRadius: '30px',
							}}
							type='submit'
							fullWidth
							variant='contained'
							sx={{ mt: 3, mb: 2 }}>
							Login
						</Button>
					</Box>
					<Button
						style={{
							backgroundColor: '#FFE2A7 ',
							color: ' #3F2525',
							borderRadius: '30px',
						}}
						onClick={() => navigate('/signup')}
						fullWidth
						variant='contained'
						sx={{ mt: 3, mb: 2 }}>
						Signup
					</Button>
				</Box>
			</Container>
		</div>
	);
}

export default Signin;
