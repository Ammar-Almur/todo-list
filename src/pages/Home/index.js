import { Divider, Grid } from '@mui/material';
import React, { useEffect } from 'react';
import ButtonCustom from '../../components/button';
import Header from '../../components/Header';
import WeatherCard from '../../components/weatherCard';
import icon1 from '../../images/Vector (1).png';
import icon2 from '../../images/carbon.png';
import Todo from '../../components/Todo';
import ModalCustom from '../../components/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { getAllTask } from '../../Redux/reducers/Todo';
function Home() {
	const dispatch = useDispatch();

	const tasks = useSelector((state) => state.todo.todos);
	useEffect(() => {
		dispatch(getAllTask());
	}, []);
	console.log('tasks', tasks);
	return (
		<div dir='ltr'>
			<Header />
			<div style={{ margin: '50px' }}>
				<WeatherCard />
				<Grid container spacing={2}>
					<Grid item xs={6} md={2}>
						<p style={{ fontSize: '2.2rem' }}>My tasks</p>
						<ButtonCustom bgColor={' #AB31D638'} icon={icon1} text={'Todo'} />
						<ButtonCustom
							bgColor={' #EFEFEF'}
							icon={icon2}
							text={'Completed'}
						/>
					</Grid>
					<Grid item xs={6} md={1} style={{ marginTop: '80px' }}>
						{' '}
						<Divider
							// variant='middle'
							orientation='vertical'
							style={{
								height: '450px',
								color: '#FFE2A7',
								width: '3px',
								margin: 'auto',
								backgroundColor: '#FFE2A7',
							}}
						/>
					</Grid>
					<Grid item xs={6} md={9} style={{ textAlign: 'center' }}>
						<div style={{ textAlign: 'right' }}>
							<ModalCustom />
						</div>
						{tasks &&
							tasks?.map((task, index) => <Todo data={task} key={task._id} />)}
					</Grid>
				</Grid>
				<div style={{ margin: '20px' }}></div>
			</div>
		</div>
	);
}

export default Home;
