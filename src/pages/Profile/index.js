import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../../components/Header';
import pen from '../../images/pen.png';
import {
	deleteUserImage,
	deleteUserProfile,
	getuserProfile,
	updateUserProfile,
	uploaUserdImg,
} from '../../Redux/reducers/Auth';
import {
	Container,
	CssBaseline,
	Box,
	FilledInput,
	Button,
	Grid,
	InputLabel,
} from '@mui/material';

import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function Profile() {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	useEffect(() => {
		dispatch(getuserProfile());
	}, []);

	const user = useSelector((state) => state.auth);
	const [image, setimage] = useState();
	const [file, setfile] = useState();
	useEffect(() => {
		user.id &&
			axios
				.get(`${process.env.REACT_APP_API_URL}/user/${user.id}/avatar`)
				.then((resp) => {
					console.log(resp);
					setimage(resp.request.responseURL);
				});
	}, [user.id]);

	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			email: data.get('email'),
			password: data.get('password'),
			name: data.get('name'),
			age: data.get('age'),
			password: data.get('password'),
		});
		dispatch(
			updateUserProfile({
				email: data.get('email'),
				password: data.get('password'),
				name: data.get('name'),
				age: data.get('age'),
				password: data.get('password'),
			})
		);
	};
	const handleDelete = () => {
		dispatch(deleteUserProfile());
	};
	const uploadImg = (e) => {
		console.log('eeeeeeeeee', e);
		setfile(e.target.files[0]);
		console.log(e.target.files[0]);
	};
	const uploade = () => {
		const d = new FormData();
		d.append('avatar', file);
		dispatch(uploaUserdImg(d));
	};
	const deleteImage = () => {
		dispatch(deleteUserImage());
	};
	return (
		<div dir='ltr'>
			<Header />

			<div style={{ textAlign: 'center' }}>
				{image ? (
					<>
						<img
							width={'150px'}
							style={{ borderRadius: '50%', position: 'relative' }}
							src={image}
							alt=''
						/>
						<button
							onClick={deleteImage}
							style={{
								width: '40px',
								height: '40px',
								borderRadius: '50%',
								border: 'none',
								top: '23%',
								left: '52%',
								backgroundColor: '#FFE2A7',
								position: 'absolute',
							}}>
							<img
								width={'30px'}
								style={{ borderRadius: '50%' }}
								src={pen}
								alt=''
							/>
						</button>
					</>
				) : (
					<Button
						variant='contained'
						component='label'
						style={{
							backgroundColor: 'rgba(0,255,255,0.9)',
							width: '150px',
							margin: ' auto',
							height: '150px',
							borderRadius: '50%',
							padding: '50px 15px',
						}}>
						Upload image
						<input onChange={uploadImg} accept='image/*' type='file' hidden />
					</Button>
				)}
				{file && (
					<Button component='label' onClick={uploade}>
						Upload
					</Button>
				)}
				{/* <img src={}></img> */}
				<h2>{user.name}</h2>
			</div>

			{/* <p>{user.name}</p>
			<p>{user.email}</p>
			<p>{user.id}</p>
			<p>{user.age}</p> */}
			<CssBaseline />
			<Box
				sx={{
					display: 'flex',
					flexDirection: 'column',
					alignItems: 'center',
				}}>
				{user.name && (
					<Box
						component='form'
						onSubmit={handleSubmit}
						noValidate
						sx={{ mt: 1 }}>
						<Grid container spacing={2}>
							<Grid item xs={6} md={6} style={{ textAlign: 'center' }}>
								<InputLabel
									style={{
										textAlign: 'left',
										marginLeft: '12%',
										color: '#AB31D6',
									}}>
									Name
								</InputLabel>
								<FilledInput
									style={{
										backgroundColor: 'white',
										marginBottom: '5px',
										width: '80%',
									}}
									margin='normal'
									name='name'
									type='text'
									id='name'
									defaultValue={user?.name}></FilledInput>
							</Grid>
							<Grid item xs={6} md={6} style={{ textAlign: 'center' }}>
								<InputLabel
									style={{
										textAlign: 'left',
										marginLeft: '12%',
										color: '#AB31D6',
									}}>
									Age
								</InputLabel>
								<FilledInput
									style={{
										backgroundColor: 'white',
										marginBottom: '5px',
										width: '80%',
									}}
									margin='normal'
									fullWidth
									name='age'
									type='number'
									id='age'
									defaultValue={user?.age}></FilledInput>
							</Grid>
							<Grid item xs={6} md={6} style={{ textAlign: 'center' }}>
								<InputLabel
									style={{
										textAlign: 'left',
										marginLeft: '12%',
										color: '#AB31D6',
									}}>
									{' '}
									New password
								</InputLabel>
								<FilledInput
									style={{
										backgroundColor: 'white',
										marginBottom: '5px',
										width: '80%',
									}}
									margin='normal'
									fullWidth
									name='password'
									type='password'
									defaultValue={user.password}
									id='password'></FilledInput>
							</Grid>
							<Grid item xs={6} md={6} style={{ textAlign: 'center' }}>
								<InputLabel
									style={{
										textAlign: 'left',
										marginLeft: '12%',
										color: '#AB31D6',
									}}>
									Enter New Password Again
								</InputLabel>
								<FilledInput
									style={{
										backgroundColor: 'white',
										marginBottom: '5px',
										width: '80%',
									}}
									margin='normal'
									fullWidth
									name='confirmpassword'
									type='password'></FilledInput>
							</Grid>
							<Grid item xs={6} md={12} style={{ textAlign: 'center' }}>
								<InputLabel
									style={{
										textAlign: 'left',
										marginLeft: '6%',
										color: '#AB31D6',
									}}>
									Email
								</InputLabel>
								<FilledInput
									style={{
										width: '90%',
										backgroundColor: 'white',
										marginBottom: '5px',
									}}
									margin='normal'
									fullWidth
									name='email'
									type='email'
									id='email'
									defaultValue={user?.email}
									autoComplete='email'></FilledInput>
							</Grid>
							<Grid item xs={6} md={6} style={{ textAlign: 'right' }}>
								<Button
									style={{
										borderRadius: '30px',
										width: '40%',
									}}
									type='submit'
									fullWidth
									variant='contained'
									sx={{ mt: 3, mb: 2 }}>
									Save
								</Button>
							</Grid>

							<Grid item xs={6} md={6} style={{ textAlign: 'left' }}>
								<Button
									style={{
										borderRadius: '30px',
										width: '40%',
										backgroundColor: '#F53333  ',
									}}
									onClick={handleDelete}
									fullWidth
									variant='contained'
									sx={{ mt: 3, mb: 2 }}>
									Delete Account
								</Button>
							</Grid>
						</Grid>
					</Box>
				)}
			</Box>
		</div>
	);
}

export default Profile;
