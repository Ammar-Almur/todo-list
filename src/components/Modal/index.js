import React from 'react';
import carbon_close from '../../images/carbon_close.png';
import plus from '../../images/akar-icons_plus.png';
import {
	Button,
	Typography,
	Fade,
	Modal,
	Box,
	Backdrop,
	TextareaAutosize,
	Container,
	Checkbox,
	InputLabel,
} from '@mui/material';
import { useDispatch } from 'react-redux';
import { addTask } from '../../Redux/reducers/Todo';
function ModalCustom() {
	const dispatch = useDispatch();
	const [open, setOpen] = React.useState(false);
	const handleOpen = () => setOpen(true);
	const handleClose = () => setOpen(false);

	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			description: data.get('description'),
			completed: data.get('completed'),
		});
		dispatch(
			addTask({
				completed: data.get('completed') ? true : false,
				description: data.get('description'),
			})
		);
		setTimeout(() => {
			setOpen(false);
		}, 2000);
	};
	const style = {
		position: 'absolute',

		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 450,
		bgcolor: 'background.paper',
		// border: '2px solid #000',
		boxShadow: 24,
		// p: 4,
		backgroundColor: 'white',
		zIndex: '10',
	};
	return (
		<>
			<Button
				onClick={handleOpen}
				sx={{ textAlign: 'left', margin: '40px  0px' }}
				endIcon={
					<img
						width={'20px'}
						height='20px'
						src={plus}
						alt=''
						style={{
							padding: '5px',
							marginLeft: 'auto',
						}}></img>
				}
				style={{
					backgroundColor: '#AB31D6  ',
					width: '15rem',
					borderRadius: '35px ',
					textAlign: 'left',
					color: 'white',
				}}>
				create new task
			</Button>
			<Modal
				aria-labelledby='transition-modal-title'
				aria-describedby='transition-modal-description'
				open={open}
				onClose={handleClose}
				closeAfterTransition
				BackdropComponent={Backdrop}
				BackdropProps={{
					timeout: 500,
				}}>
				<Fade in={open}>
					<Box style={style} dir='ltr'>
						<div
							style={{
								display: 'flex',
								borderBottom: '1px solid #AB31D6',
								padding: '5px 20px',
							}}>
							<Typography
								sx={{ mt: 1 }}
								id='transition-modal-title'
								variant='h6'
								component='h2'>
								Create New Task
							</Typography>
							<div style={{ textAlign: 'right', width: '60%' }}>
								<Button
									onClick={handleClose}
									endIcon={
										<img width={'35px'} src={carbon_close} alt=''></img>
									}></Button>
							</div>
						</div>
						<Container>
							<Box
								component='form'
								onSubmit={handleSubmit}
								noValidate
								style={{ text: 'center', padding: '25px' }}
								sx={{ mt: 1 }}>
								<InputLabel
									style={{
										margin: '0px 10px -30px 10px',
										backgroundColor: 'white',
										width: 'fit-content',
										color: '#AB31D6',
										fontSize: '0.8rem',
									}}>
									Task Description
								</InputLabel>
								<TextareaAutosize
									aria-label='minimum height'
									name='description'
									id='description'
									minRows={14}
									placeholder=''
									style={{
										fontSize: '1.2rem',
										maxWidth: '100%',
										margin: ' 20px auto 50px auto',
										width: '100%',
										border: '1px solid #AB31D6',
										padding: '12px',
									}}
								/>
								Completed:
								<Checkbox defaultChecked name='completed' id='completed' />
								<br />
								<Button
									style={{
										borderRadius: '30px',
										width: '8rem',
										margin: '40px 10px 20px 10px',
									}}
									type='submit'
									fullWidth
									variant='contained'
									sx={{ mt: 3, mb: 2 }}>
									Save
								</Button>
								<Button
									onClick={handleClose}
									style={{
										borderRadius: '30px',
										width: '8rem',
										margin: '40px 10px 20px 10px',
										backgroundColor: '#FFE2A7',
										color: 'black',
									}}
									fullWidth
									variant='contained'
									sx={{ mt: 3, mb: 2 }}>
									Close
								</Button>
							</Box>
						</Container>
					</Box>
				</Fade>
			</Modal>
		</>
	);
}

export default ModalCustom;
