import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import img from '../../images/Rectangle 4.png';
import avatar from '../../images/Ellipse 1.png';
import { useDispatch } from 'react-redux';
import { logout } from '../../Redux/reducers/Auth';
import { useNavigate } from 'react-router-dom';
const settings = ['Profile', 'Logout'];

const Header = () => {
	const [anchorElNav, setAnchorElNav] = React.useState(null);
	const [anchorElUser, setAnchorElUser] = React.useState(null);
	const dispatch = useDispatch();
	const navigate = useNavigate();
	const handleOpenNavMenu = (event) => {
		setAnchorElNav(event.currentTarget);
	};
	const handleOpenUserMenu = (event) => {
		setAnchorElUser(event.currentTarget);
	};

	const handleCloseNavMenu = () => {
		setAnchorElNav(null);
	};

	const handleCloseUserMenu = () => {
		setAnchorElUser(null);
	};
	const handleLogout = () => {
		dispatch(logout());
	};

	return (
		<AppBar
			position=''
			dir='ltr'
			sx={{ backgroundColor: 'white', boxShadow: 'none' }}>
			<Container maxWidth='xl'>
				<Toolbar>
					<a href='/'>
						<img alt='Remy Sharp' width={'60px'} src={img} />
					</a>

					<Box sx={{ flexGrow: 1, textAlign: 'right' }}>
						<Tooltip title='Open settings'>
							<IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
								<Avatar alt='Remy Sharp' src={avatar} />
							</IconButton>
						</Tooltip>
						<Menu
							sx={{ mt: '45px' }}
							id='menu-appbar'
							anchorEl={anchorElUser}
							anchorOrigin={{
								vertical: 'top',
								horizontal: 'center',
							}}
							keepMounted
							transformOrigin={{
								vertical: 'top',
								horizontal: 'center',
							}}
							open={Boolean(anchorElUser)}
							onClose={handleCloseUserMenu}>
							<MenuItem key={''} onClick={handleCloseUserMenu}>
								<Typography
									onClick={() => navigate('/profile')}
									textAlign='center'>
									Profile
								</Typography>
							</MenuItem>
							<MenuItem key={''} onClick={handleCloseUserMenu}>
								<Typography textAlign='center' onClick={handleLogout}>
									Logout
								</Typography>
							</MenuItem>
						</Menu>
					</Box>
				</Toolbar>
			</Container>
		</AppBar>
	);
};
export default Header;
