import React, { useEffect } from 'react';
import { Grid, Button } from '@mui/material';
import Box from '@mui/material/Box';
import Vector from '../../images/Vector.png';
import carbon from '../../images/carbon_task-complete.png';
import clarity from '../../images/clarity_date-line.png';
import garbage from '../../images/garbage.png';
import EditModalCustom from '../EditModal';
import edit from '../../images/c.png';
import { useDispatch } from 'react-redux';
import { deleteTask, getAllTask } from '../../Redux/reducers/Todo';
function Todo({ data }) {
	const dispatch = useDispatch();

	const handleDeleteTask = () => {
		dispatch(deleteTask({ id: data._id }));
		setTimeout(() => {
			dispatch(getAllTask());
		}, 1000);
	};
	console.log('task is', data.data);

	var date = () => {
		var d = new Date(data?.updatedAt);
		const y = d.getFullYear();
		const m = d.getMonth();
		const day = d.getDay();
		return y + '/' + m + '/' + day;
		console.log('date is ', y, m, day);
	};

	return (
		<div>
			{' '}
			<Box sx={{ flexGrow: 1 }}>
				<Grid container spacing={2} columns={16}>
					<Grid xs={8} md={2} sx={{ mt: 3 }}>
						<img src={Vector} alt=''></img>
					</Grid>
					<Grid xs={8} md={11} style={{ textAlign: 'left' }}>
						<h2>{data?.description}</h2>
						<div
							style={{
								display: 'flex',
								margin: '5px 0',
								alignItems: 'center',
								alignContent: 'center',
							}}>
							<img src={clarity} alt='' style={{ margin: '0px 10px' }}></img>
							<span>creation date : </span>
							<span> {date()}</span>
						</div>
						<div style={{ margin: '15px 0', display: 'flex' }}>
							<span>
								<img src={carbon} alt='' style={{ margin: '0px 10px' }}></img>
							</span>
							<span>completed : </span>
							<span> {data?.completed ? 'true' : 'false'}</span>
						</div>
					</Grid>
					<Grid xs={8} md={3} sx={{ mt: 7 }} style={{ textAlign: 'right' }}>
						<span>
							<EditModalCustom id={data?._id} />
							{/* <Button  >
								{' '}
								<img src={edit} alt='' style={{ margin: '0 25px' }}></img>
							</Button> */}

							<Button onClick={handleDeleteTask}>
								<img src={garbage} alt='' style={{ margin: '0 25px' }}></img>
							</Button>
						</span>
					</Grid>
				</Grid>
			</Box>
		</div>
	);
}

export default Todo;
