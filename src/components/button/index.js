import React from 'react';
import { Button } from '@mui/material';
import { useDispatch } from 'react-redux';
import { getCompletedOrPaginationTask } from '../../Redux/reducers/Todo';
function ButtonCustom({ icon, text, bgColor }) {
	const dispatch = useDispatch();
	const handleApi = () => {
		text == 'Todo' &&
			dispatch(getCompletedOrPaginationTask({ limit: 20, skip: 2 }));
		text == 'Completed' &&
			dispatch(getCompletedOrPaginationTask({ completed: true }));
	};
	return (
		<div>
			<Button
				onClick={handleApi}
				sx={{ textAlign: 'left', margin: '20px  0px' }}
				startIcon={
					<img
						width={'20px'}
						height='20px'
						src={icon}
						alt=''
						style={{
							padding: '10px',
							marginLeft: 'auto',
						}}></img>
				}
				style={{
					backgroundColor: bgColor,
					width: '15rem',
					borderRadius: '10px ',
					textAlign: 'left',
				}}>
				<span style={{ margin: '0px 0px 0px 0px' }}>{text}</span>
			</Button>
		</div>
	);
}

export default ButtonCustom;
