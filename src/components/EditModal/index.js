import React, { useEffect, useState } from 'react';
import carbon_close from '../../images/carbon_close.png';
import plus from '../../images/akar-icons_plus.png';
import edit from '../../images/c.png';
import {
	Button,
	Typography,
	Fade,
	Modal,
	Box,
	Backdrop,
	TextareaAutosize,
	Container,
	Checkbox,
	InputLabel,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import {
	addTask,
	editTask,
	getTask,
	getAllTask,
} from '../../Redux/reducers/Todo';
function EditModalCustom({ id }) {
	const [task, settask] = useState(null);
	const dispatch = useDispatch();
	const [open, setOpen] = React.useState(false);
	const handleOpen = () => {
		settask(null);
		dispatch(getTask(id));
		setOpen(true);
	};
	const handleClose = () => setOpen(false);

	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			completed: data.get('completed'),
		});
		dispatch(
			editTask({ id: id, completed: data.get('completed') ? true : false })
		);
		setTimeout(() => {
			dispatch(getAllTask());
			setOpen(false);
		}, 1000);
	};
	const taskDetailes = useSelector((state) => state.todo.todo);
	useEffect(() => {
		settask(taskDetailes);
	}, [taskDetailes]);

	console.log('task is by id ', task);
	const style = {
		position: 'absolute',

		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: 450,
		bgcolor: 'background.paper',
		// border: '2px solid #000',
		boxShadow: 24,
		// p: 4,
		backgroundColor: 'white',
		zIndex: '10',
	};
	return (
		<>
			<Button onClick={handleOpen}>
				{' '}
				<img src={edit} alt='' style={{ margin: '0 25px' }}></img>
			</Button>
			<Modal
				aria-labelledby='transition-modal-title'
				aria-describedby='transition-modal-description'
				open={open}
				onClose={handleClose}
				closeAfterTransition
				BackdropComponent={Backdrop}
				BackdropProps={{
					timeout: 500,
				}}>
				<Fade in={open}>
					<Box style={style} dir='ltr'>
						<div
							style={{
								display: 'flex',
								borderBottom: '1px solid #AB31D6',
								padding: '5px 20px',
							}}>
							<Typography
								sx={{ mt: 1 }}
								id='transition-modal-title'
								variant='h6'
								component='h2'>
								Edit Task
							</Typography>
							<div style={{ textAlign: 'right', width: '80%' }}>
								<Button
									onClick={handleClose}
									endIcon={
										<img width={'35px'} src={carbon_close} alt=''></img>
									}></Button>
							</div>
						</div>
						<Container>
							<Box
								component='form'
								onSubmit={handleSubmit}
								noValidate
								style={{ text: 'center', padding: '25px' }}
								sx={{ mt: 1 }}>
								<InputLabel
									style={{
										margin: '0px 10px -30px 10px',
										backgroundColor: 'white',
										width: 'fit-content',
										color: '#AB31D6',
										fontSize: '0.8rem',
									}}>
									Task Description
								</InputLabel>
								<TextareaAutosize
									disabled
									aria-label='minimum height'
									name='description'
									id='description'
									minRows={14}
									placeholder=''
									style={{
										fontSize: '1.2rem',
										maxWidth: '100%',
										margin: ' 20px auto 50px auto',
										width: '100%',
										border: '1px solid #AB31D6',
										padding: '12px',
									}}
									defaultValue={task?.description}
								/>
								Completed:
								{task && (
									<Checkbox
										name='completed'
										id='completed'
										defaultChecked={task?.completed}
									/>
								)}
								<br />
								<Button
									style={{
										borderRadius: '30px',
										width: '8rem',
										margin: '40px 10px 20px 10px',
									}}
									type='submit'
									fullWidth
									variant='contained'
									sx={{ mt: 3, mb: 2 }}>
									Edit
								</Button>
								<Button
									onClick={handleClose}
									style={{
										borderRadius: '30px',
										width: '8rem',
										margin: '40px 10px 20px 10px',
										backgroundColor: '#FFE2A7',
										color: 'black',
									}}
									fullWidth
									variant='contained'
									sx={{ mt: 3, mb: 2 }}>
									Close
								</Button>
							</Box>
						</Container>
					</Box>
				</Fade>
			</Modal>
		</>
	);
}

export default EditModalCustom;
