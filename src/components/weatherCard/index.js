import React, { useEffect, useState } from 'react';
import { Card, Box, Container } from '@mui/material';
import cloud from '../../images/undraw_weather_app_re_kcb1 (1) 1.png';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { getuserProfile } from '../../Redux/reducers/Auth';
function WeatherCard() {
	const dispatch = useDispatch();
	const [weather, setweather] = useState();
	const [lat, setLat] = useState([]);
	const [long, setLong] = useState([]);
	// let location = window.navigator.geolocation();

	useEffect(() => {
		navigator.geolocation.getCurrentPosition(function (position) {
			setLat(position.coords.latitude);
			setLong(position.coords.longitude);
		});

		console.log('Latitude is:', lat);
		console.log('Longitude is:', long);
	}, [lat, long]);
	useEffect(() => {
		axios
			.get(
				`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=aa10af87ee47d7576eabae61f160b5e3`
			)
			.then((res) => {
				setweather(res.data);
			});
	}, [lat, long]);

	useEffect(() => {
		dispatch(getuserProfile());
	}, []);
	const name = useSelector((state) => state.auth.name);

	console.log('wwwwwwwww', weather);
	return (
		<div dir='ltr'>
			<Card
				sx={{
					display: 'flex',

					padding: '30px',
					borderRadius: '25px',
					backgroundColor: ' #FFE2A7  ',
					border: 'none',
				}}>
				<Box>
					<h2>Welocome back,</h2>
					<h2>{name && name}!</h2>
				</Box>
				<Box sx={{ flexGrow: 1, textAlign: 'right' }}>
					<h3>{weather && weather.weather[0].main} ,</h3>
					<h3>{weather && Math.floor(weather.main.temp - 273.15)} C</h3>
				</Box>
				<img src={cloud} alt=''></img>
			</Card>
		</div>
	);
}

export default WeatherCard;
