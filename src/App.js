import './App.css';
import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Signin from './pages/Signin';
import Signup from './pages/Signup';
import Home from './pages/Home';
import Profile from './pages/Profile';

// *************************************************

function App() {
	return (
		<>
			{' '}
			<BrowserRouter>
				<Routes>
					<Route
						path='/'
						element={localStorage.getItem('token') ? <Home /> : <Signin />}
					/>
					<Route path='/home' element={<Home />} />
					<Route path='/signup' element={<Signup />} />
					<Route path='/profile' element={<Profile />} />

					{/* <Route path='/addlibrary' element={<AddLibrary />} /> */}
				</Routes>
			</BrowserRouter>
		</>
	);
}

export default App;
